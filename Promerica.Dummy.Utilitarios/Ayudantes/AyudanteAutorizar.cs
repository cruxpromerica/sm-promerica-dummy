﻿using Comunes.Pruebas;
using Promerica.Dummy.Modelos.Contenedores;
using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promerica.Dummy.Utilitarios.Ayudantes
{
    public class AyudanteAutorizar : AyudanteBase<EscenarioAutorizar>
    {
        public override List<EscenarioAutorizar> ListaEscenarios
        {
            get
            {
                return _listaEscenarios;
            }
        }

        public AyudanteAutorizar()
        {
            _listaEscenarios = new ObtenerEscenarios().ObtenerListaEscenariosDesdeArchivo<EscenarioAutorizar>(Recursos.Recursos.Prueba_Autorizar_DummyPromerica);
        }

        public Respuesta Autorizar(Pago pago)
        {
            Respuesta respuesta = null;
            EscenarioAutorizar escenario = ListaEscenarios.FirstOrDefault(e => e.CodReferencia == pago.CodReferencia);
            respuesta = new Respuesta { CodEstado = escenario.CodEstado, CodMotivoRechazo = escenario.CodMotivoRechazo, Descripcion = escenario.Descripcion };
            return respuesta;
        }
    }
}
