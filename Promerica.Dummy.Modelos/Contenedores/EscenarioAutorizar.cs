﻿using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Promerica.Dummy.Modelos.Contenedores
{
    public class EscenarioAutorizar : EscenarioBase
    {
        [DataMember]
        public string CodReferencia { get; set; }

        [DataMember]
        public Moneda Moneda { get; set; }

        [DataMember]
        public decimal Monto { get; set; }

        [DataMember]
        public int NumTelefonoDestino { get; set; }

        [DataMember]
        public int NumTelefonoOrigen { get; set; }

        [DataMember]
        public string IdClienteOrigen { get; set; }

        [DataMember]
        public string NombreClienteOrigen { get; set; }

        [DataMember]
        public EstadoOperacion CodEstado { get; set; }

        [DataMember]
        public int? CodMotivoRechazo { get; set; }

        [DataMember]
        public string Descripcion { get; set; }
    }
}