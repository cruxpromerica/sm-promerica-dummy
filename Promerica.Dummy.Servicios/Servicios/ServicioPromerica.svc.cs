﻿using Promerica.Dummy.Contratos;
using Promerica.Dummy.Utilitarios.Ayudantes;
using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Promerica.Dummy.Servicios
{
    public class ServicioPromerica : IServicioPromerica
    {
        public Respuesta Autorizar(Pago pago)
        {
            var ayudante = new AyudanteAutorizar();
            return ayudante.Autorizar(pago);
        }

        public void Confirmar(Pago pago)
        {
            
        }

        public void Rechazar(Pago pago)
        {
            
        }

        public void Inactivar(Suscripcion pago)
        {
            
        }
    }
}
