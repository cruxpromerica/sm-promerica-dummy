﻿using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Promerica.Dummy.Contratos
{
    [ServiceContract]
    public interface IServicioPromerica
    {
        [OperationContract]
        Respuesta Autorizar(Pago pago);

        [OperationContract]
        void Confirmar(Pago pago);

        [OperationContract]
        void Rechazar(Pago pago);

        [OperationContract]
        void Inactivar(Suscripcion pago);
    }
}